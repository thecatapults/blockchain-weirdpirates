## Description

API for bring support of ERC1155 to any backend.

## Installation

```bash
$ npm install
```

## Configuration
Create a `.env` file in this folder with the following content. 
```
MNEMONIC_PRIVATEKEY=mother dutch trim early exhaust stone blind blossom solar final bulk crawl
NODE_RPC=https://goerli.infura.io/v3/8e2abfa8af334ae68207d13fd0843804
TOKEN_ADDRESS=0xd0489D0591E7b88a1F6F03e49B501A7C3d6A0841
MEGAS_OF_MEMORY_AVAILABLE=200
```
* **MNEMONIC_PRIVATEKEY**: Admin private key phrase seed, used to mint objects, update objects URI and relay transfers.
* **NODE_RPC**: URL of our node JSON-RPC endpoint.
* **TOKEN_ADDRESS**: Address of the token that this service will administrate.
* **MEGAS_OF_MEMORY_AVAILABLE**: Set how much memory can this service use
> Change those values for production environments

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
After run it you can visit [localhost:3000/docs](http://localhost:3000/docs) to see Swagger docs.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```