import { Controller, Post, Put, Get, Body, Query } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import { AccountManagerService } from './services/AccountManager.service';
import { CreateUserAccountBatchDto, CreateUserAccountDto } from './dto/CreateUserAccount.dto';
import { UpdateUserAccountPassphraseDto } from './dto/UpdateUserAccountPassphrase.dto';
import { CreateObjectDtoDto } from './dto/CreateObject.dto';
import { ObjectsManagerService } from './services/ObjectsManager.service';
import { TransferObjectDto } from './dto/TransferObject.dto';
import { GetBalanceDto } from './dto/GetBalance.dto';
import { UpdateURIDto } from './dto/UpdateURI.dto';
import { BatchTransferObjectDto } from './dto/BatchTransferObject.dto';

@Controller()
export class AppController {
  constructor(
    private readonly accountManager: AccountManagerService,
    private readonly objectsManager: ObjectsManagerService,
  ) {}

  @Post('user')
  @ApiTags('User Account')
  @ApiCreatedResponse({
    description: 'New user address',
    type: String,
  })
  createUserAccount(@Body() params: CreateUserAccountDto): Promise<string> {
    return this.accountManager.createUserAccount(params);
  }

  @Post('userBatch')
  @ApiTags('User Account')
  @ApiOkResponse({
    description: 'Create many user accounts',
    type: CreateUserAccountBatchDto,
  })
  async batchCreateUserAccount(@Body() params: CreateUserAccountBatchDto) {
    return this.accountManager.batchCreateUserAccount(params.items);
  }

  @Put('user/passphrase')
  @ApiTags('User Account')
  @ApiOkResponse({
    type: Boolean,
  })
  updateUserAccountPassphrase(
    @Body() params: UpdateUserAccountPassphraseDto,
  ): Promise<boolean> {
    return this.accountManager.updateUserAccountPhassprease(params);
  }

  @Get('user')
  @ApiTags('User Account')
  @ApiOkResponse({
    description: 'User account address',
    type: String,
  })
  getUserAccountAddress(@Query('userId') userId: string): Promise<string> {
    return this.accountManager.getUserAccountAddress(userId);
  }

  @Post('object')
  @ApiTags('Objects')
  @ApiOkResponse({
    description: 'Transaction hash of creation',
    type: String,
  })
  createObject(@Body() params: CreateObjectDtoDto): Promise<string> {
    return this.objectsManager.createObject(params);
  }

  @Get('balance')
  @ApiTags('Objects')
  @ApiOkResponse({
    description: 'User account balance',
    type: String,
  })
  getBalance(@Query() balanceDto: GetBalanceDto): Promise<string> {
    return this.objectsManager.getBalance(balanceDto);
  }

  @Put('updateURI')
  @ApiTags('Objects')
  @ApiOkResponse({
    description: 'Update URI',
    type: String,
  })
  async updateURI(@Body() params: UpdateURIDto): Promise<string> {
    return this.objectsManager.updateURI(params);
  }

  @Post('transfer')
  @ApiTags('Transfer Objects')
  @ApiOkResponse({
    description: 'Transaction hash of transfer',
    type: String,
  })
  async transferObject(@Body() params: TransferObjectDto): Promise<string> {
    return this.objectsManager.transferObject(params);
  }

  @Post('batchTransfer')
  @ApiTags('Transfer Objects')
  @ApiOkResponse({
    description: 'Transaction hash of batch transfer',
    type: String,
  })
  async batchTransfer(@Body() params: BatchTransferObjectDto) {
    return this.objectsManager.batchTransfer(params.items);
  }
}
