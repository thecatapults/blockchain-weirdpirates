import { Test, TestingModule } from '@nestjs/testing';
import { AccountManagerService } from './AccountManager.service';
import { ethers } from 'ethers';

describe('AccountManagerService', () => {
  let service: AccountManagerService;

  beforeEach(async () => {
    service = new AccountManagerService()
  });

  const userId = Math.random().toString(),
    passphrase = 'test',
    newPassphrase = 'test2';
  let userWalletAddress;
  describe('Account Managment', () => {
    describe('Create Account', () => {
      it('Create account should be possible', async () => {
        const response = await service.createUserAccount({userId, passphrase});
        expect(ethers.utils.isAddress(response)).toBe(true);
        userWalletAddress = response
      });
      it('Create account for equal userId should fails', async () => {
        const call = () => service.createUserAccount({userId, passphrase});
        await expect(call).rejects.toThrow('Account for giving userId already exist');
      });
    });
    describe('Update Account Passphrase', () => {
      it('Fails when original passphrase is wrong', async () => {
        const call = () => service.updateUserAccountPhassprease({userId, oldPassphrase: newPassphrase, newPassphrase: passphrase});
        await expect(call).rejects.toThrow('Invalid wallet passphrase');
      })
      it('Update passphrase should be possible', async () => {
        const response = await service.updateUserAccountPhassprease({userId, oldPassphrase: passphrase, newPassphrase});
        expect(response).toEqual(true);
      })
    });
    describe('Get user wallet address', () => {
      it('Get user wallet address should response a valid address', async () => {
        const response = await service.getUserAccountAddress(userId);
        expect(response).toEqual(userWalletAddress);
      });
    });
  });
});
