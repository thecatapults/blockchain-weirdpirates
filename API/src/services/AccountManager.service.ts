import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ethers } from 'ethers';
import * as fs from 'fs';
import * as path from 'path';
import { UpdateUserAccountPassphraseDto } from '../dto/UpdateUserAccountPassphrase.dto';
import {
  CreateUserAccountBatchResponse,
  CreateUserAccountDto,
} from '../dto/CreateUserAccount.dto';

@Injectable()
export class AccountManagerService {
  private async fileExist(path: string) {
    return new Promise((resolve) => {
      fs.access(path, fs.constants.F_OK, (err) => {
        resolve(err === null);
      });
    });
  }

  private getWalletPath(userId: string): string {
    return path.join(__dirname, '..', '..', 'accounts', userId);
  }

  private async getWalletContent(userId: string): Promise<string> {
    const walletPath = this.getWalletPath(userId),
      walletFileExist = await this.fileExist(walletPath);
    if (!walletFileExist) {
      throw new HttpException(
        `Wallet do not exist for ${userId}`,
        HttpStatus.FORBIDDEN,
      );
    }
    const walletContent = await fs.promises.readFile(walletPath);
    return walletContent.toString();
  }

  private async getWallet(
    userId: string,
    passphrase: string,
  ): Promise<ethers.Wallet> {
    const walletContent = await this.getWalletContent(userId);
    let wallet;
    try {
      wallet = await ethers.Wallet.fromEncryptedJson(walletContent, passphrase);
    } catch (error) {
      throw new HttpException(
        `Invalid wallet passphrase for user ${userId}`,
        HttpStatus.FORBIDDEN,
      );
    }
    return wallet;
  }

  public async createUserAccount({
    userId,
    passphrase,
  }: CreateUserAccountDto): Promise<string> {
    const walletPath = this.getWalletPath(userId),
      walletFileExist = await this.fileExist(walletPath);

    if (walletFileExist) {
      throw new HttpException(
        'Account for giving userId already exist',
        HttpStatus.FORBIDDEN,
      );
    }

    const wallet = ethers.Wallet.createRandom(),
      walletEncryptedJson = await wallet.encrypt(passphrase);
    await fs.promises.writeFile(walletPath, walletEncryptedJson);
    return wallet.address;
  }

  public async batchCreateUserAccount(
    params: CreateUserAccountDto[],
  ): Promise<CreateUserAccountBatchResponse[]> {
    const wallets = await Promise.all(
      params.map((p) => this.createUserAccount(p)),
    );
    return wallets.map((address, index) => ({
      userId: params[index].userId,
      address,
    }));
  }

  public async updateUserAccountPhassprease({
    userId,
    oldPassphrase,
    newPassphrase,
  }: UpdateUserAccountPassphraseDto): Promise<boolean> {
    const walletPath = this.getWalletPath(userId),
      wallet = await this.getWallet(userId, oldPassphrase),
      walletEncryptedJson = await wallet.encrypt(newPassphrase);
    await fs.promises.writeFile(walletPath, walletEncryptedJson);
    return true;
  }

  public async getUserAccountAddress(userId: string): Promise<string> {
    const walletContent = await this.getWalletContent(userId),
      walletObject = JSON.parse(walletContent);
    return ethers.utils.getAddress(walletObject['address']);
  }

  public async signMessage(
    userId: string,
    passphrase: string,
    message: ethers.utils.Bytes,
  ): Promise<string> {
    const wallet = await this.getWallet(userId, passphrase);
    return wallet.signMessage(message);
  }
}
