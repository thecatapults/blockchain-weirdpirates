import { Injectable } from '@nestjs/common';
import { CreateObjectDtoDto } from '../dto/CreateObject.dto';
import { BigNumber, ethers } from 'ethers';
import {
  WeirdPiratesObjects,
  WeirdPiratesObjects__factory,
} from '../contracts';
import { TransferObjectDto } from '../dto/TransferObject.dto';
import { AccountManagerService } from './AccountManager.service';
import { GetBalanceDto } from '../dto/GetBalance.dto';
import { UpdateURIDto } from '../dto/UpdateURI.dto';
import { from } from 'rxjs';

@Injectable()
export class ObjectsManagerService {
  public readonly ObjectsContractAddress: WeirdPiratesObjects;
  public readonly Provider: ethers.providers.BaseProvider;
  public readonly AccountManager: AccountManagerService;
  private AdminNonce: number;
  constructor() {
    this.Provider = new ethers.providers.JsonRpcProvider(
      process.env['NODE_RPC'],
    );
    this.ObjectsContractAddress = WeirdPiratesObjects__factory.connect(
      process.env['TOKEN_ADDRESS'],
      this.Provider,
    );
    this.AccountManager = new AccountManagerService();
  }
  private async signAdminTransaction(
    transaction: ethers.providers.TransactionRequest,
  ): Promise<string> {
    const wallet = ethers.Wallet.fromMnemonic(
      process.env['MNEMONIC_PRIVATEKEY'],
    );
    const populatedTransaction = await wallet
      .connect(this.Provider)
      .populateTransaction(transaction);
    return wallet.signTransaction(populatedTransaction);
  }
  public async createObject({
    userId,
    objectId,
    amount,
  }: CreateObjectDtoDto): Promise<string> {
    const to = await this.AccountManager.getUserAccountAddress(userId),
      transaction = await this.ObjectsContractAddress.populateTransaction.createObject(
        to,
        objectId,
        amount,
        '0x00',
      ),
      signedTransaction = await this.signAdminTransaction(transaction);
    return this.sendTransaction(signedTransaction);
  }
  public async transferObject({
    fromUserId,
    fromUserAccountPassphrase,
    toUserId,
    objectId,
    amount,
  }: TransferObjectDto): Promise<string> {
    const from = await this.AccountManager.getUserAccountAddress(fromUserId),
      to = await this.AccountManager.getUserAccountAddress(toUserId),
      nonce = await this.ObjectsContractAddress.accountNonce(from),
      data = '0x00',
      contractAddress = this.ObjectsContractAddress.address,
      messageHash = ethers.utils.arrayify(
        ethers.utils.solidityKeccak256(
          [
            'address',
            'address',
            'uint256',
            'uint256',
            'bytes',
            'address',
            'uint256',
          ],
          [from, to, objectId, amount, data, contractAddress, nonce],
        ),
      ),
      signature = await this.AccountManager.signMessage(
        fromUserId,
        fromUserAccountPassphrase,
        messageHash,
      ),
      transaction = await this.ObjectsContractAddress.populateTransaction.relayedSafeTransferFrom(
        from,
        to,
        objectId,
        amount,
        data,
        contractAddress,
        nonce,
        signature,
      ),
      signedTransaction = await this.signAdminTransaction(transaction);
    return this.sendTransaction(signedTransaction);
  }

  public async getBalance(balanceDto: GetBalanceDto): Promise<string> {
    const account = await this.AccountManager.getUserAccountAddress(
      balanceDto.userId,
    );
    const balance = await this.ObjectsContractAddress.balanceOf(
      account,
      balanceDto.objectId,
    );
    return balance.toString();
  }

  private async sendTransaction(signedTransaction: string): Promise<string> {
    const transactionReceipt = await this.Provider.sendTransaction(
      signedTransaction,
    );
    await transactionReceipt.wait(1);
    return transactionReceipt.hash;
  }

  public async updateURI({ uri }: UpdateURIDto): Promise<string> {
    const transaction = await this.ObjectsContractAddress.populateTransaction.updateURI(
      uri,
    );
    const signedTransaction = await this.signAdminTransaction(transaction);
    return this.sendTransaction(signedTransaction);
  }


  public async batchTransfer(items: TransferObjectDto[]): Promise<any> {
    const datas = [];
    const nonces = [];
    const fromTransactions = [];
    const toTransactions = [];
    const amounts = [];
    const objectsIds = [];
    const signatures = [];
    const contractAddress = this.ObjectsContractAddress.address;
    for (const item of items) {
      amounts.push(item.amount);
      objectsIds.push(item.objectId);
      const from = await this.AccountManager.getUserAccountAddress(
        item.fromUserId,
      );
      fromTransactions.push(from);
      const to = await this.AccountManager.getUserAccountAddress(item.toUserId);
      toTransactions.push(to);
      const nonce = await this.ObjectsContractAddress.accountNonce(from);
      const data = '0x00';
      datas.push(data);
      const messageHash = ethers.utils.arrayify(
        ethers.utils.solidityKeccak256(
          [
            'address',
            'address',
            'uint256',
            'uint256',
            'bytes',
            'address',
            'uint256',
          ],
          [from, to, item.objectId, item.amount, data, contractAddress, nonce],
        ),
      );
      nonces.push(nonce);
      const signature = await this.AccountManager.signMessage(
        item.fromUserId,
        item.fromUserAccountPassphrase,
        messageHash,
      );
      signatures.push(signature);
    }
    console.log(fromTransactions, 'array from');
    console.log(toTransactions, 'array to');
    console.log(objectsIds, 'array objects');
    console.log(amounts, 'array amounts');
    console.log(nonces, 'array nonces');
    console.log(signatures, 'array signatures');

    const transaction = await this.ObjectsContractAddress.populateTransaction.relayedSafeBatchTransferFrom(
      fromTransactions,
      toTransactions,
      objectsIds,
      amounts,
      datas,
      contractAddress,
      nonces,
      signatures,
    );
    const signedTransaction = await this.signAdminTransaction(transaction);
    return this.sendTransaction(signedTransaction);
  }
}
