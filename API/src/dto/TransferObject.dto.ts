import {ApiProperty} from "@nestjs/swagger";

export class TransferObjectDto {
  @ApiProperty()
  fromUserId: string;
  @ApiProperty()
  fromUserAccountPassphrase: string;
  @ApiProperty()
  toUserId: string;
  @ApiProperty()
  objectId: number;
  @ApiProperty()
  amount: number;
}