import {ApiProperty} from "@nestjs/swagger";

export class UpdateUserAccountPassphraseDto {
  @ApiProperty()
  userId: string;
  @ApiProperty()
  oldPassphrase: string;
  @ApiProperty()
  newPassphrase: string;
}