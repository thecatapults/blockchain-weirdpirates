import { ApiProperty } from '@nestjs/swagger';

export class UpdateURIDto {
  @ApiProperty()
  uri: string;
}
