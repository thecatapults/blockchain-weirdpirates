import { ApiProperty } from '@nestjs/swagger';

export class CreateObjectDtoDto {
  @ApiProperty({
    description: 'User id that will receive token balance',
  })
  userId: string;

  @ApiProperty({
    description: 'Id of the object',
  })
  objectId: number;

  @ApiProperty({
    description: 'Amount that will be minted',
  })
  amount: number;
}