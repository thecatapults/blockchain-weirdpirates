import {ApiProperty} from "@nestjs/swagger";
import {TransferObjectDto} from "./TransferObject.dto";

export class BatchTransferObjectDto {
  @ApiProperty({
    isArray: true,
    type: TransferObjectDto,
  })
  items: TransferObjectDto[];
}