import { ApiProperty } from '@nestjs/swagger';

export class GetBalanceDto {
  @ApiProperty()
  userId: string;
  @ApiProperty()
  objectId: number;
}
