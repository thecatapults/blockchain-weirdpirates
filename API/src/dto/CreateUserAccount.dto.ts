import { ApiProperty } from '@nestjs/swagger';
import { TransferObjectDto } from './TransferObject.dto';

export class CreateUserAccountDto {
  @ApiProperty()
  userId: string;
  @ApiProperty()
  passphrase: string;
}

export class CreateUserAccountBatchDto {
  @ApiProperty({
    isArray: true,
    type: CreateUserAccountDto,
  })
  items: CreateUserAccountDto[];
}

export interface CreateUserAccountBatchResponse {
  userId: string;
  address: string;
}
