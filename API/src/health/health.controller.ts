import { Controller, Get } from '@nestjs/common';
import {
  HealthCheckService,
  MemoryHealthIndicator,
  HealthCheck,
} from '@nestjs/terminus';
import { ApiTags } from '@nestjs/swagger';

const availableMegas = parseInt(
  process.env['MEGAS_OF_MEMORY_AVAILABLE'] || '200',
);

@Controller('health')
@ApiTags('Health')
export class HealthController {
  constructor(
    private health: HealthCheckService,
    private memory: MemoryHealthIndicator,
  ) {}

  @Get()
  @HealthCheck()
  check() {
    return this.health.check([
      async () =>
        this.memory.checkHeap('memory_heap', availableMegas * 1024 * 1024),
    ]);
  }
}
