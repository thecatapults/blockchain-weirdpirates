import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TerminusModule } from '@nestjs/terminus';
import { AccountManagerService } from './services/AccountManager.service';
import { ObjectsManagerService } from './services/ObjectsManager.service';
import { HealthController } from './health/health.controller';

@Module({
  imports: [TerminusModule],
  controllers: [AppController, HealthController],
  providers: [AccountManagerService, ObjectsManagerService],
})
export class AppModule {}
