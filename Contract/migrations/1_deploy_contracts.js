const WeirdPiratesObjects = artifacts.require('WeirdPiratesObjects'),
    { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

module.exports = async function(deployer) {
    await deployProxy(WeirdPiratesObjects, [''], { deployer });
};
