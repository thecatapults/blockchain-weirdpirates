require('dotenv').config();
const HDWalletProvider = require("@truffle/hdwallet-provider");

const mnemonic_privatekey = process.env["MNEMONIC_PRIVATEKEY"],
    network_id = process.env["NETWORK_ID"],
    node_rpc = process.env["NODE_RPC"],
    network_gas_price = process.env["NETWORK_GAS_PRICE"] || null;

module.exports = {
  networks: {
    local: {
      network_id: "6660001",
      gasPrice: network_gas_price,
      provider: () => new HDWalletProvider(mnemonic_privatekey, "http://bicha.lab.riaquest.com:8545")
    },
    public: {
      network_id: network_id,
      gasPrice: network_gas_price,
      provider: () =>  new HDWalletProvider(mnemonic_privatekey, node_rpc)
    }
  },
  compilers: {
    solc: {
      version: '0.6.12',
      settings: {
        optimizer: {
          enabled: true, // Default: false
          runs: 200	  // Default: 200
        }
      }
    }
  }
};
