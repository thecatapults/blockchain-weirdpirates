// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts-upgradeable/proxy/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/cryptography/ECDSA.sol";

contract WeirdPiratesObjects is Initializable, ERC1155Upgradeable, OwnableUpgradeable {

    address private _relayedSender;

    using ECDSA for bytes32;

    event UpdatedURI(string uri);

    address private constant ADDRESS_ZERO = address(0);

    mapping(address => uint256) private _accountsNonce;

    function initialize(string memory uri_) public initializer {
        _relayedSender = ADDRESS_ZERO;
        __ERC1155_init(uri_);
        __Ownable_init();
    }

    function updateURI(string memory uri_) external onlyOwner {
        _setURI(uri_);
        emit UpdatedURI(uri_);
    }

    function createObject(address account, uint256 id, uint256 amount, bytes memory data) external onlyOwner {
        _mint(account, id, amount, data);
    }

    function createObjects(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) external onlyOwner {
        _mintBatch(to, ids, amounts, data);
    }


    function accountNonce(address account) public view returns (uint256) {
        return _accountsNonce[account];
    }

    function relayedSafeTransferFrom(address from, address to, uint256 id, uint256 amount, bytes memory data, address contractAddress, uint256 nonce, bytes memory signature) public {
        require(contractAddress == address(this), "RelayedSafeTransferFrom: Invalid contract address");
        require(nonce == _accountsNonce[from], "RelayedSafeTransferFrom: Invalid nonce");
        bytes memory blob = abi.encodePacked(
            from,
            to,
            id,
            amount,
            data,
            contractAddress,
            nonce
        );
        require(keccak256(blob).toEthSignedMessageHash().recover(signature) == from, "RelayedSafeTransferFrom: Invalid signature");
        _relayedSender = from;
        _accountsNonce[from] = _accountsNonce[from].add(1);
        this.safeTransferFrom(from, to, id, amount, data);
        _relayedSender = ADDRESS_ZERO;
    }

    function relayedSafeBatchTransferFrom(address[] memory from, address[] memory to, uint256[] memory id, uint256[] memory amount, bytes[] memory data, address contractAddress, uint256[] memory nonce, bytes[] memory signature) public {
        require(
            from.length == to.length
            &&
            to.length == id.length
            &&
            id.length == amount.length
            &&
            amount.length == data.length
            &&
            data.length == nonce.length
            &&
            nonce.length == signature.length, "relayedSafeBatchTransferFrom: Invalid arguments length");
        for (uint256 index; index < from.length; index++) {
            relayedSafeTransferFrom(from[index], to[index], id[index], amount[index], data[index], contractAddress, nonce[index], signature[index]);
        }
    }

    function _msgSender() internal view override returns (address payable) {
        if (_relayedSender == ADDRESS_ZERO) {
            return msg.sender;
        } else {
            return payable(_relayedSender);
        }
    }
}