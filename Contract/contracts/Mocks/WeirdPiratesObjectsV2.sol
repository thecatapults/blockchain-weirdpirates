// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;
pragma experimental ABIEncoderV2;

import "../WeirdPiratesObjects.sol";

contract WeirdPiratesObjectsV2 is WeirdPiratesObjects {

	function newMethod() public pure returns (string memory) {
		return "Version 2";
	}

}