const WeirdPiratesObjects = artifacts.require('WeirdPiratesObjects'),
    WeirdPiratesObjectsV2 = artifacts.require('WeirdPiratesObjectsV2'),
    { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades'),
    truffleAssert = require('truffle-assertions')

contract('WeirdPiratesObjects', ([owner, ...accounts]) => {

    before(async () => {
        this.proxy = await deployProxy(WeirdPiratesObjects, ['']);
        this.token = await WeirdPiratesObjects.at(this.proxy.address)
        this.proxyAdmin = accounts[accounts.length -1 ]
    })

    describe('Update URi', () => {
        it('Third party should not update the URI', async () => {
            await truffleAssert.reverts(
                this.token.updateURI("test", {
                    from: accounts[0]
                }),
                'Ownable: caller is not the owner'
            )
        })
        it('Owner should update the URI', async () => {
            const uri = 'test'
            await truffleAssert.passes(
                this.token.updateURI(uri, {
                    from: owner
                })
            )
            const newUri = await this.token.uri(0)
            assert.equal(uri, newUri)
        })
        it('Update the URI should emit an event', async () => {
            const uri = 'test',
                result = await this.token.updateURI(uri, {
                    from: owner
                })
            truffleAssert.eventEmitted(result, 'UpdatedURI', (ev) => {
                return ev.uri === uri
            });
        })
    })

    describe('Create Objects', () => {
        describe('Single', () => {
            before(() => {
                this.object = {
                    id: 0,
                    units: 1
                }
            })
            it('Third party should not create an object', async () => {
                await truffleAssert.reverts(
                    this.token.createObject(accounts[0], this.object.id, this.object.units, "0x0", {
                        from: accounts[0]
                    }),
                    'Ownable: caller is not the owner'
                )
            })
            it('Owner should create an object', async () => {
                await truffleAssert.passes(
                    this.token.createObject(accounts[0], this.object.id, this.object.units, "0x0", {
                        from: owner
                    })
                )
                const accountBalance = await this.token.balanceOf(accounts[0], this.object.id)
                assert.equal(this.object.units.toString(), accountBalance.toString())
            })
            it('Third party should not create new units of an object', async () => {
                await truffleAssert.reverts(
                    this.token.createObject(accounts[0], this.object.id, this.object.units, "0x0", {
                        from: accounts[0]
                    }),
                    'Ownable: caller is not the owner'
                )
            })
            it('Owner should create new units of an object', async () => {
                await truffleAssert.passes(
                    this.token.createObject(accounts[0], this.object.id, this.object.units, "0x0", {
                        from: owner
                    })
                )
                const accountBalance = await this.token.balanceOf(accounts[0], this.object.id)
                assert.equal((this.object.units + 1).toString(), accountBalance.toString())
            })
        })
        describe('Batch', () => {
            before(() => {
                this.objects = {
                    ids: [1, 2],
                    units: [3, 5]
                }
            })
            it('Third party should not create objects', async () => {
                await truffleAssert.reverts(
                    this.token.createObjects(accounts[0], this.objects.ids, this.objects.units, "0x0", {
                        from: accounts[0]
                    }),
                    'Ownable: caller is not the owner'
                )
            })
            it('Owner should create objects', async () => {
                await truffleAssert.passes(
                    this.token.createObjects(accounts[0], this.objects.ids, this.objects.units, "0x0", {
                        from: owner
                    })
                )
                const accountBalances = await Promise.all(
                    this.objects.ids.map(id => this.token.balanceOf(accounts[0], id))
                )
                accountBalances.map((balance, id) => {
                    assert.equal(
                        this.objects.units[id].toString(),
                        balance.toString()
                    )
                })
            })
            it('Third party should not create new units of objects', async () => {
                await truffleAssert.reverts(
                    this.token.createObjects(accounts[0], this.objects.ids, this.objects.units, "0x0", {
                        from: accounts[0]
                    }),
                    'Ownable: caller is not the owner'
                )
            })
            it('Owner should create new units of objects', async () => {
                await truffleAssert.passes(
                    this.token.createObjects(accounts[0], this.objects.ids, [1, 1], "0x0", {
                        from: owner
                    })
                )
                const accountBalance = await Promise.all(
                    this.objects.ids.map(id => this.token.balanceOf(accounts[0], id))
                )
                accountBalance.map((balance, id) => {
                    assert.equal(
                        (this.objects.units[id] + 1).toString(),
                        balance.toString()
                    )
                })
            })
        })
    })

    describe("relaySafeTransferFrom", () => {
        before(async () => {
            this.account = web3.eth.accounts.create()
            await this.token.createObject(this.account.address, 3, 2, '0x0')
            const from = this.account.address,
                to = accounts[4],
                id = 3,
                amount = 1,
                data = '0x00',
                contractAddress = this.token.address,
                nonce = 0,
                messageHash = web3.utils.soliditySha3(
                    {
                        t: 'address',
                        v: from
                    }, {
                        t: 'address',
                        v: to
                    }, {
                        t: 'uint256',
                        v: id
                    }, {
                        t: 'uint256',
                        v: amount
                    }, {
                        t: 'bytes',
                        v: data
                    }, {
                        t: 'address',
                        v: contractAddress
                    }, {
                        t: 'uint256',
                        v: nonce
                    }
                ),
                {signature} = this.account.sign(messageHash)

            this.transaction = {
                from,
                to,
                id,
                amount,
                data,
                contractAddress,
                nonce,
                signature
            }
        })

        it("Invalid from should be reverted", async () => {
            await truffleAssert.reverts(
                this.token.relayedSafeTransferFrom(
					accounts[4],
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
                    this.transaction.contractAddress,
                    this.transaction.nonce,
                    this.transaction.signature
                ),
                'RelayedSafeTransferFrom: Invalid signature'
            )
        })

        it("Invalid contractAddress should be reverted", async () => {
            await truffleAssert.reverts(
                this.token.relayedSafeTransferFrom(
                    this.transaction.from,
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
					accounts[4],
                    this.transaction.nonce,
                    this.transaction.signature
                ),
                'RelayedSafeTransferFrom: Invalid contract address'
            )
        })

        it("Invalid nonce should be reverted", async () => {
            await truffleAssert.reverts(
                this.token.relayedSafeTransferFrom(
                    this.transaction.from,
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
                    this.transaction.contractAddress,
                    this.transaction.nonce + 1,
                    this.transaction.signature
                ),
                'RelayedSafeTransferFrom: Invalid nonce'
            )
        })

        it("Invalid signature should be reverted", async () => {
            const {signature} = this.account.sign('0x00')
            await truffleAssert.reverts(
                this.token.relayedSafeTransferFrom(
                    this.transaction.from,
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
                    this.transaction.contractAddress,
                    this.transaction.nonce,
                    signature
                ),
                'RelayedSafeTransferFrom: Invalid signature'
            )
        })

        it("Valid signature should be succeeded", async () => {
            const previousBalanceFrom = await this.token.balanceOf(this.transaction.from, this.transaction.id),
                previousBalanceTo = await this.token.balanceOf(this.transaction.to, this.transaction.id)
            assert.equal(previousBalanceFrom.toString(), '2')
            assert.equal(previousBalanceTo.toString(), '0')
            await truffleAssert.passes(
                this.token.relayedSafeTransferFrom(
                    this.transaction.from,
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
                    this.transaction.contractAddress,
                    this.transaction.nonce,
                    this.transaction.signature
                )
            )
            const currentBalanceFrom = await this.token.balanceOf(this.transaction.from, this.transaction.id),
                currentBalanceTo = await this.token.balanceOf(this.transaction.to, this.transaction.id)
            assert.equal(currentBalanceFrom.toString(), '1')
            assert.equal(currentBalanceTo.toString(), '1')
        })

        it("Account nonce should be updated", async () => {
            const accountNonce = await this.token.accountNonce(this.transaction.from)
            assert.equal(accountNonce.toString(), '1')
        })

        it("Relay transaction with updated nonce should succeeded", async () => {
            const messageHash = web3.utils.soliditySha3(
                {
                    t: 'address',
                    v: this.transaction.from
                }, {
                    t: 'address',
                    v: this.transaction.to
                }, {
                    t: 'uint256',
                    v: this.transaction.id
                }, {
                    t: 'uint256',
                    v: this.transaction.amount
                }, {
                    t: 'bytes',
                    v: this.transaction.data
                }, {
                    t: 'address',
                    v: this.transaction.contractAddress
                }, {
                    t: 'uint256',
                    v: this.transaction.nonce + 1
                }
                ),
                {signature} = this.account.sign(messageHash)
            await truffleAssert.passes(
                this.token.relayedSafeTransferFrom(
                    this.transaction.from,
                    this.transaction.to,
                    this.transaction.id,
                    this.transaction.amount,
                    this.transaction.data,
                    this.transaction.contractAddress,
                    this.transaction.nonce + 1,
                    signature
                )
            )
        })

        it("Relay batch transactions should succeeded", async () => {
            const localAccounts = [web3.eth.accounts.create(), web3.eth.accounts.create()],
                from = [localAccounts[0].address, localAccounts[1].address],
                to = [accounts[4], accounts[4]],
                id = [0,0],
                amount = [1,1],
                data = ['0x00','0x00'],
                contractAddress = this.transaction.contractAddress,
                nonce = [0, 0],
                messageHash_0 = web3.utils.soliditySha3(
                {
                    t: 'address',
                    v: from[0]
                }, {
                    t: 'address',
                    v: to[0]
                }, {
                    t: 'uint256',
                    v: id[0]
                }, {
                    t: 'uint256',
                    v: amount[0]
                }, {
                    t: 'bytes',
                    v: data[0]
                }, {
                    t: 'address',
                    v: contractAddress
                }, {
                    t: 'uint256',
                    v: nonce[0]
                }),
                {signature: signature_0} = localAccounts[0].sign(messageHash_0),
                messageHash_1 = web3.utils.soliditySha3(
                    {
                        t: 'address',
                        v: from[1]
                    }, {
                        t: 'address',
                        v: to[1]
                    }, {
                        t: 'uint256',
                        v: id[1]
                    }, {
                        t: 'uint256',
                        v: amount[1]
                    }, {
                        t: 'bytes',
                        v: data[1]
                    }, {
                        t: 'address',
                        v: contractAddress
                    }, {
                        t: 'uint256',
                        v: nonce[1]
                    }),
                {signature: signature_1} = localAccounts[1].sign(messageHash_1)

            await this.token.createObject(localAccounts[0].address, 0, 1, '0x0')
            await this.token.createObject(localAccounts[1].address, 0, 1, '0x0')

            await truffleAssert.passes(
                this.token.relayedSafeBatchTransferFrom(from, to, id, amount, data, this.transaction.contractAddress, nonce, [signature_0, signature_1])
            )
        })
    })

    describe("Upgrade implementation", () => {
        it("Upgrade should be possible", async () => {
            await truffleAssert.passes(upgradeProxy(
                this.proxy.address,
                WeirdPiratesObjectsV2
            ))

            this.token = await WeirdPiratesObjectsV2.at(this.proxy.address)
                
            const newMethodResponse = await this.token.newMethod()
            assert.equal(newMethodResponse, 'Version 2')
        })

        describe('Basic functionality after migration', () => {
            it('Owner should update the URI', async () => {
                const uri = 'after_migration'
                await truffleAssert.passes(
                    this.token.updateURI(uri, {
                        from: owner
                    })
                )
                const newUri = await this.token.uri(0)
                assert.equal(uri, newUri)
            })
            it('Owner should create an object', async () => {
                await truffleAssert.passes(
                    this.token.createObject(accounts[0], 35, 1, "0x0", {
                        from: owner
                    })
                )
                const accountBalance = await this.token.balanceOf(accounts[0], 35)
                assert.equal('1', accountBalance.toString())
            })
        })
    })
})
