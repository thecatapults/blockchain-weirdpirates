## Description

Constract based on ERC1155 with some support for minting and relaying methods. 

## Installation

```bash
$ npm install
```

## Configuration
Create a `.env` file in this folder with the following content. 
```
MNEMONIC_PRIVATEKEY=mother dutch trim early exhaust stone blind blossom solar final bulk crawl
NODE_RPC=https://goerli.infura.io/v3/8e2abfa8af334ae68207d13fd0843804
NETWORK_ID=5
```
* **MNEMONIC_PRIVATEKEY** Private key word seed of the Owner and deployer of this contracts.
* **NODE_RPC** JsonRPC endpoint of the network where will deploy
* **NETWORK_ID** Id of the network where will deploy
* Optionally you can set a value for `NETWORK_GAS_PRICE` 
> Change those values for production environments

## Deploy

```bash
# Migrate contracts to a public network, defined with .env
$ npm run migrate
```

## Test

```bash
# unit tests
$ npm run test
```