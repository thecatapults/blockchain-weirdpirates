# Flows
Este `API`permite gestionar las wallets de los usuarios, gestionar los objetos y transferir objetos entra las wallets de los usuarios.
## Gestion de usuarios
### Crear usuario: `POST /user`  
**Body:**
```json
{
  "userId": "string",
  "passphrase": "string"
}
```
Donde `userId` es un identificador propuesto por el sistema y `passphrase` es la contraseña con la que el servicio encriptara la `wallet` que genere, el hehco es que el `API` no conoce esta contraseña por lo cual sera dato requerido de todas las operaciones que requieran de la firma de dicha `wallet`.  

**Responde:** El address de la nueva wallet para el usuario.

### Consultar direccion de la wallet de un usuario: `GET /user`
Parametros:
```typescript
userId: string
```
**Responde:** El address de la nueva wallet para el usuario.

### Cambiar contraseña de la wallet de un usuario: `PUT /user/phass`ç
**Body:**
```json
{
  "userId": "string",
  "oldPassphrase": "string",
  "newPassphrase": "string"
}
```
## Gestion de Objetos
### Crear un objeto: `POST /object`
**Body:**
````json
{
  "objectId": 0,
  "amount": 0,
  "userId": "string"
}
````
`objectId` sera el id del objeto que se creara, `amount` es la cantidad que se creara de ese objeto y `userId` de quien recibira esas unidades recien creadas.  
Este metodo sirve para crear nuevos objetos o mas unidades de cualquiera existente.  
> Este como todos los request que requieran una transaccion contra la blockchain espera a que dicha transaccion tenga al menos 1 confirmación. Esto genera que el tiempo de respuesta sea mayor de lo normal.  

**Responde:** El hash de la trasaccion en la blockchain, se puede consultar con [etherscan.io](https://etherscan.io)

### Transferir objetos existentes: `POST /transfer`
**Body:**
```json
{
  "fromUserId": "string",
  "fromUserAccountPassphrase": "string",
  "toUserId": "string",
  "objectId": 0,
  "amount": 0
}
```
Esta operacion requiere la firma de `fromUserId` por esto la necesidad de `fromUserAccountPassphrase`  
**Responde:** El hash de la trasaccion en la blockchain

### Consultar el balance de un usuario: `GET /balance`
**Parametros:**
```typescript
userId: string
objectId: number
```
**Responde:** El balance del usuario para el tokenId dado

### Transferecia de objetos en lote: `POST /batchTransfer`
**Parametros:**
```json
{
  "items": [
    {
      "fromUserId": "string",
      "fromUserAccountPassphrase": "string",
      "toUserId": "string",
      "objectId": 0,
      "amount": 0
    }
  ]
}
```
Ejecuta en una sola transaccion todas las transferencias listadas, en principio soportaria hasta 5 transferencias en simultaneo pero esto depende del estado de la red.  
**Responde:** El hash de la trasaccion en la blockchain